package com.disney.sample.challenge.db.remote

import com.disney.sample.challenge.common.api.ApiService
import com.disney.sample.challenge.common.helper.BaseDataSource
import com.disney.sample.challenge.common.helper.Resource
import com.disney.sample.challenge.db.entities.ComicStoriesResponse

class RemoteDataSource(private val apiService: ApiService) : BaseDataSource() {

    /* API to access comic stories. */
    suspend fun getComicStories(): Resource<ComicStoriesResponse> =
        getResult { apiService.getComicStories() }

}
