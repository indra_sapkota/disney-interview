package com.disney.sample.challenge.db

import com.disney.sample.challenge.common.helper.executeDataAccess
import com.disney.sample.challenge.common.helper.executeNetworkCall
import com.disney.sample.challenge.db.entities.ComicStoriesResponse
import com.disney.sample.challenge.db.local.LocalDataDao
import com.disney.sample.challenge.db.remote.RemoteDataSource

class DataRepository constructor(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataDao
) {

    /** Retrieve Comic stories. */
    fun getComicStories(waitForServer: Boolean) = executeDataAccess(
        databaseQuery = { localDataSource.getComicStories() },
        networkCall = { remoteDataSource.getComicStories() },
        saveCallResult = { updateComicLocalDatabase(response = it) },
        waitForServer = waitForServer
    )

    /* Update local database with new data from server. */
    private suspend fun updateComicLocalDatabase(response: ComicStoriesResponse) {
        if (response.data.results.isNullOrEmpty()) return
        localDataSource.insertComicStories(response.data.results)
    }

    suspend fun getComicStoriesSync() = executeNetworkCall(
        networkCall = { remoteDataSource.getComicStories() }
    )

}
