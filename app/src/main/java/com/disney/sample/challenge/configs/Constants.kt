package com.disney.sample.challenge.configs

import java.math.BigInteger
import java.security.MessageDigest
import java.sql.Timestamp


/** Application Configurations Constants. */
object Constants {

    const val DATABASE_NAME = "Disney_Database"
    val ts = Timestamp(System.currentTimeMillis()).time.toString()

    const val API_BASE_URL = "http://gateway.marvel.com/v1/public/"

    const val API_KEY = "<API_KEY>"
    const val PRIVATE_KEY = "<PRIVATE_KEY>"

    fun hash(): String {
        val input = "$ts$PRIVATE_KEY$API_KEY"
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(input.toByteArray())).toString(16).padStart(32, '0')
    }

}