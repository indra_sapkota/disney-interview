package com.disney.sample.challenge.db.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.disney.sample.challenge.db.entities.*

@Dao
interface LocalDataDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertComicStories(comics: List<ComicStories>)

    @Query("SELECT * FROM comics ORDER BY title DESC")
    fun getComicStories(): LiveData<List<ComicStories>>
}
