package com.disney.sample.challenge.ui.base

import android.os.StrictMode
import android.view.WindowManager
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableBoolean
import androidx.databinding.ViewDataBinding

/** Abstract base activity to manage the VideDataBinding for AppCompactActivity. */
abstract class CoreActivity<T : CoreActivity<T, DB>, DB : ViewDataBinding> : AppCompatActivity() {
    var loading = ObservableBoolean(false)
    var activity: T? = null
    var binding: DB? = null

    open fun showLoading() {
        runOnUiThread {
            loading.set(true)
            window?.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
            )
        }
    }

    open fun hideLoading() {
        runOnUiThread {
            loading.set(false)
            window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }

    open fun setDefaults(activity: T, @LayoutRes layoutRes: Int) {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        this.activity = activity
        binding = DataBindingUtil.setContentView(activity, layoutRes)
        binding?.lifecycleOwner = activity
    }
}
