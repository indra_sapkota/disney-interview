package com.disney.sample.challenge.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableBoolean
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

/** Abstract base fragment to manage the VideDataBinding for Fragments. */
abstract class CoreFragment<T : CoreFragment<T, DB>, DB : ViewDataBinding> : Fragment() {
    var loading = ObservableBoolean(false)
    var binding: DB? = null
    private var rootView: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? = getRootView(inflater, container)

    private fun getRootView(inflater: LayoutInflater, container: ViewGroup?): View? {
        if (rootView == null) {
            binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false)
            rootView = binding!!.root
        }
        return rootView
    }

    open fun showLoading() {
        requireActivity().runOnUiThread {
            loading.set(true)
            activity?.window?.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
            )
        }
    }

    open fun hideLoading() {
        requireActivity().runOnUiThread {
            loading.set(false)
            activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }

    @LayoutRes
    abstract fun getLayoutRes(): Int

}
