package com.disney.sample.challenge.ui

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.disney.sample.challenge.R
import com.disney.sample.challenge.databinding.ActivityMainBinding
import com.disney.sample.challenge.ui.base.CoreActivity
import com.disney.sample.challenge.ui.comic.ComicViewModel

class MainActivity : CoreActivity<MainActivity, ActivityMainBinding>() {

    private lateinit var navController: NavController
    private lateinit var comicViewModel: ComicViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setDefaults(this, R.layout.activity_main)
        binding?.vm = this

        // Setup navigation controller
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fragment_container) as NavHostFragment
        val navController = navHostFragment.navController

        // Initialize View Model
        comicViewModel = ViewModelProvider(this).get(ComicViewModel::class.java)
    }
}
