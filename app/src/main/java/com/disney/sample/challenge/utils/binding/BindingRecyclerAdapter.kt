package com.disney.sample.challenge.utils.binding

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filterable
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BindingRecyclerAdapter<T, U : ViewDataBinding?> :
    RecyclerView.Adapter<BindingRecyclerAdapter<T, U>.MyViewHolder>(), Filterable {

    private var layoutID = 0

    /** @param layoutID item layout ID. */
    open fun bindAdapterView(@LayoutRes layoutID: Int) {
        this.layoutID = layoutID
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: U = DataBindingUtil.inflate(inflater, viewType, parent, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item: T = getItemForPosition(position)
        holder.bind(item)
        setView(holder.binding, position)
    }


    /** @return pojo model for current item. */
    protected abstract fun getItemForPosition(position: Int): T

    /** extra method to perform extra operations on list items. */
    protected abstract fun setView(binding: U, position: Int)

    override fun getItemViewType(position: Int): Int {
        return getLayoutIdForPosition(position)
    }

    protected abstract fun getLayoutIdForPosition(position: Int): Int

    /** @return give Binding variable name here. */
    abstract fun getBindingVariableID(): Int

    inner class MyViewHolder(val binding: U) : RecyclerView.ViewHolder(binding?.root!!) {
        fun bind(item: T?) {
            binding?.setVariable(getBindingVariableID(), item)
            binding?.executePendingBindings()
        }
    }

}
