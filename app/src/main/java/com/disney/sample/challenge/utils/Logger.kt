package com.disney.sample.challenge.utils

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.viewbinding.BuildConfig

/**
 * Utils class to Logging application log into console.
 * @sample debug(tag: String, message: String) Used to log debug level logging, only available on debug version of build.
 *          Logger.debug(tag, message)
 * @sample warning(tag: String, message: String) Used for log warnings level logging!
 *          Logger.warning(tag, message)
 * @sample error(tag: String, message: String) Used for log errors level logging!
 *          Logger.warning(tag, message, exception: Throwable?)
 * */

object Logger {

    fun info(tag: String?, message: String) {
        if (BuildConfig.DEBUG)
            Log.i(tag, message)
    }

    fun debug(tag: String?, message: String) {
        if (BuildConfig.DEBUG)
            Log.d(tag, message)
    }

    fun warning(tag: String?, message: String) {
        Log.w(tag, message)
    }

    fun error(tag: String?, message: String, exception: Throwable?) {
        Log.e(tag, message, exception)
    }

    fun timeStamp(tag: String?, component: String, time: Long) {
        if (BuildConfig.DEBUG) {
            Log.v(tag, "[TimeStamp] $component ${
                String.format("%.2f ms", 1.0f.times(time.div(1_000_000)))}"
            )
        }
    }

    fun toastMessage(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}