package com.disney.sample.challenge.utils.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.disney.sample.challenge.R

@BindingAdapter("vAdapter")
fun vListAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>?) {
    val manager = LinearLayoutManager(recyclerView.context, LinearLayoutManager.VERTICAL, false)
    recyclerView.setHasFixedSize(true)
    recyclerView.layoutManager = manager
    recyclerView.adapter = adapter
}

@BindingAdapter("hAdapter")
fun hListAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>?) {
    val manager = LinearLayoutManager(recyclerView.context, LinearLayoutManager.HORIZONTAL, false)
    recyclerView.setHasFixedSize(true)
    recyclerView.layoutManager = manager
    recyclerView.adapter = adapter
}

@BindingAdapter(value = ["gAdapter", "column"], requireAll = false)
fun gListAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>?, column: Int) {
    val gridLayoutManager = GridLayoutManager(recyclerView.context, if (column > 0) column else 3)
    recyclerView.setHasFixedSize(true)
    recyclerView.layoutManager = gridLayoutManager
    recyclerView.adapter = adapter
}

@BindingAdapter(value = ["previewImage"], requireAll = false)
fun previewImage(imageView: ImageView, url: String?) {
    val options: RequestOptions = RequestOptions()
        .centerCrop()
        .placeholder(R.drawable.progress_animation)
        .error(R.drawable.ic_disney_plus_logo)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .priority(Priority.HIGH)
        .dontAnimate()
        .dontTransform()
    // Load Image
    Glide.with(imageView.context).load(url).apply(options).into(imageView)
}

