package com.disney.sample.challenge.db.entities

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class ComicDataConverters {

    @TypeConverter
    fun fromThumbnail(value: Thumbnail): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toThumbnail(value: String): Thumbnail {
        val type: Type = object : TypeToken<Thumbnail>() {}.type
        return Gson().fromJson(value, type)
    }

    @TypeConverter
    fun fromThumbnails(value: List<Thumbnail>): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toThumbnails(value: String): List<Thumbnail> {
        val type: Type = object : TypeToken<Thumbnail>() {}.type
        return Gson().fromJson(value, type)
    }

}
