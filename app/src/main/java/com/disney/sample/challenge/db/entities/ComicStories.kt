package com.disney.sample.challenge.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName

data class ComicStoriesResponse(
    val code: String,
    val status: String,
    val copyright: String,
    val attributionText: String,
    val attributionHTML: String,
    val data: ComicStoriesData,
    val etag: String
)

data class ComicStoriesData(
    val offset: String,
    val limit: String,
    val total: String,
    val count: String,
    val results: List<ComicStories>
)

@Entity(tableName = "comics")
@TypeConverters(ComicDataConverters::class)
data class ComicStories(
    @PrimaryKey val id: String,
    @SerializedName("digitalId")
    val digitalID: String,
    val title: String,
    val issueNumber: String,
    val description: String,
    val pageCount: String,
    val thumbnail: Thumbnail,
    val images: List<Thumbnail>,
)

data class Thumbnail(
    val path: String,
    val extension: String
)

