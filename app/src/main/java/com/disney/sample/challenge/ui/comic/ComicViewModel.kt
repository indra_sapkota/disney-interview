package com.disney.sample.challenge.ui.comic

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.disney.sample.challenge.common.RetrofitApiBuilder
import com.disney.sample.challenge.db.AppDatabase
import com.disney.sample.challenge.db.DataRepository
import com.disney.sample.challenge.db.remote.RemoteDataSource
import com.disney.sample.challenge.utils.Logger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ComicViewModel(context: Application) : AndroidViewModel(context) {

    private val tagName = ComicViewModel::class.java.simpleName

    private var repository: DataRepository

    init {
        Logger.info(tagName, "ComicViewModel initialized ...")
        val remoteDataSource = RemoteDataSource(RetrofitApiBuilder.apiService)
        val localDataSource = AppDatabase.getDatabase(context).comicStoriesDao()
        repository = DataRepository(remoteDataSource, localDataSource)

        // Query Comic Stories When view model created
        viewModelScope.launch(Dispatchers.IO) {
            repository.getComicStories(true)
        }
    }

    /** Query Comics Stories. */
    fun queryComicStories(waitForServer: Boolean) = repository.getComicStories(waitForServer)

    suspend fun queryComicStoriesSync() = repository.getComicStoriesSync()
}