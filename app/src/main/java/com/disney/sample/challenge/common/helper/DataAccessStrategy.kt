package com.disney.sample.challenge.common.helper

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/** Execute synchronous api call to do network api request. */
suspend fun <T> executeNetworkCall(networkCall: suspend () -> Resource<T>): Resource<T> =
    withContext(Dispatchers.IO) { return@withContext networkCall.invoke() }

/** Execute Asynchronous api call to server plus returns local cache if exists. */
fun <T, A> executeDataAccess(
    databaseQuery: () -> LiveData<T>,
    networkCall: suspend () -> Resource<A>,
    saveCallResult: suspend (A) -> Unit,
    waitForServer: Boolean
) = liveData(Dispatchers.IO) {
    emit(Resource.loading())
    val source = databaseQuery.invoke().map { Resource.success(it) }
    if (!waitForServer) emitSource(source)

    val response = networkCall.invoke()
    if (response.status == Resource.Status.SUCCESS) {
        saveCallResult(response.data!!)
        emitSource(source)

    } else if (response.status == Resource.Status.ERROR) {
        emit(Resource.error(response.message!!))
        emitSource(source)
    }
}
