package com.disney.sample.challenge.common.api

import com.disney.sample.challenge.configs.Constants
import com.disney.sample.challenge.db.entities.ComicStoriesResponse
import retrofit2.Response
import retrofit2.http.*
import java.sql.Timestamp

interface ApiService {

    @GET("comics")
    suspend fun getComicStories(
        @Query("ts") timeStamp: String = Constants.ts,
        @Query("apikey") apikey: String = Constants.API_KEY,
        @Query("hash") hash: String = Constants.hash()
    ): Response<ComicStoriesResponse>
}
