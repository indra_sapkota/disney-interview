package com.disney.sample.challenge

import android.app.Application
import com.disney.sample.challenge.utils.Logger

class DisneyApplication: Application() {

    companion object {
        private val tagName = DisneyApplication::class.java.simpleName
    }

    override fun onCreate() {
        super.onCreate()
        Logger.info(tagName, "[DisneyApplication] onCreate()")
    }

    override fun onTerminate() {
        super.onTerminate()
        Logger.info(tagName, "[DisneyApplication] onTerminate()")
    }
}