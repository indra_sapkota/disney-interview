package com.disney.sample.challenge.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.disney.sample.challenge.configs.Constants
import com.disney.sample.challenge.db.entities.ComicStories
import com.disney.sample.challenge.db.local.LocalDataDao

@Database(
    version = 1,
    entities = [ComicStories::class],
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun comicStoriesDao(): LocalDataDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase = instance ?: synchronized(this) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, AppDatabase::class.java, Constants.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }

}
