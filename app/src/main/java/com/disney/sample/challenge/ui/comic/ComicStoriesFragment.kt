package com.disney.sample.challenge.ui.comic

import android.os.Bundle
import android.view.View
import android.widget.Filter
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.disney.sample.challenge.BR
import com.disney.sample.challenge.R
import com.disney.sample.challenge.databinding.FragmentComicsBinding
import com.disney.sample.challenge.databinding.ItemComicStoryBinding
import com.disney.sample.challenge.db.AppDatabase
import com.disney.sample.challenge.db.entities.ComicStories
import com.disney.sample.challenge.db.local.LocalDataDao
import com.disney.sample.challenge.ui.base.CoreFragment
import com.disney.sample.challenge.utils.Logger
import com.disney.sample.challenge.utils.binding.BindingRecyclerAdapter
import kotlinx.android.synthetic.main.fragment_comics.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/** [Fragment] to loads all comics stories and handle navigation to previous/next. */
class ComicStoriesFragment : CoreFragment<ComicStoriesFragment, FragmentComicsBinding>() {

    private val tagName = ComicStoriesFragment::class.java.simpleName
    private var comicViewModel: ComicViewModel? = null

    var index: ObservableField<Int> = ObservableField(0)
    var total: ObservableField<Int> = ObservableField(0)

    private val comicStoriesDao: LocalDataDao
        get() = AppDatabase.getDatabase(requireContext()).comicStoriesDao()

    var comicStories: MutableList<ComicStories> = mutableListOf()
    var comicAdapter: ObservableField<BindingRecyclerAdapter<ComicStories, ItemComicStoryBinding>> =
        ObservableField()

    override fun getLayoutRes(): Int = R.layout.fragment_comics
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.vm = this
        Logger.info(tagName, "onViewCreated()")

        // Register ViewModel
        comicViewModel = activity?.run { ViewModelProvider(this).get(ComicViewModel::class.java) }
            ?: throw Exception("Comic view model has not been registered on activity")
        Logger.info(tagName, "ComicViewModel() initialized")

        // Load Comic Stories data
        createComicStoriesViewAdapter()
        lifecycleScope.launch(Dispatchers.Main) {
            val data = comicViewModel?.queryComicStoriesSync()
            Logger.info(tagName, "queryComicStoriesSync() $data")
            comicStories = data?.data?.data?.results?.toMutableList() ?: mutableListOf()
            comicAdapter.get()?.notifyDataSetChanged()
            total.set(comicStories.size)
        }

        // Observe dataset changes from local database
        comicStoriesDao.getComicStories().observe(viewLifecycleOwner, { comics ->
            Logger.info(tagName, "getComicStories()")
            comicStories.clear()
            if (comics.isNotEmpty()) comicStories.addAll(comics)
            comicAdapter.get()?.notifyDataSetChanged()
            refreshComicStories.isRefreshing = false
        })

        // Handle Data Refresh
        view.findViewById<SwipeRefreshLayout>(R.id.refreshComicStories).setOnRefreshListener {
            comicViewModel?.queryComicStories(false)
            refreshComicStories.isRefreshing = false
        }
    }


    /** Observer to update List Adapter for TV Shows Episodes. */
    private fun createComicStoriesViewAdapter() {
        Logger.info(tagName, "createComicStoriesViewAdapter()")
        comicAdapter.set(object : BindingRecyclerAdapter<ComicStories, ItemComicStoryBinding>() {
            override fun setView(binding: ItemComicStoryBinding, position: Int) {
                val story = getItemForPosition(position)
                binding.comic = story
                index.set(position)
            }

            override fun getLayoutIdForPosition(position: Int): Int = R.layout.item_comic_story
            override fun getItemForPosition(position: Int): ComicStories = comicStories[position]
            override fun getBindingVariableID(): Int = BR.vm
            override fun getItemCount(): Int = comicStories.size
            override fun getFilter(): Filter? = null
        })
    }

    fun previousComicStory() {
        val indexCount = index.get()
        if (indexCount != null && indexCount > 1 && indexCount <= comicStories.size) {
            val position = indexCount - 1
            Logger.info(tagName, "loading position: $position")
            rvComicShows.smoothScrollToPosition(position)
            index.set(position)
        }
    }

    fun nextComicStory() {
        val indexCount = index.get()
        if (indexCount != null && indexCount >= 0 && indexCount < comicStories.size) {
            val position = indexCount + 1
            Logger.info(tagName, "loading position: $position")
            rvComicShows.smoothScrollToPosition(position)
            index.set(position)
        }
    }

}