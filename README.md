# Disney Coding Challenge (Android Interview)

Uses MVVM + Jetpack Architecture component to develop application

#### Commit Project code::
  * Project workspace setup and added project requirement docs folder
  * Added dependencies required to project
  * Create account in marvel developer portal
  * Generate Private and Public key + gather information to generate the hash key for api authentication
  * Added the Local database for caching using room database
  * Added network layer using repository pattern to access data from server (Retrofit)
  * Design UI/UX based on the provided requirement design screenshots
  * Created Activity/Fragment for UI (with base CoreAcitivity and CoreFragment + databinding)
  * Created ComicStorieViewModel for handling business model
  * Created Constant configuration to manage configs
  * Created Logger helper for logging application info

#### Used Library
  * Architecture: MVVM + ViewModel
  * ViewModel + Coroutine
  * Retrofit: Network library
  * Room: local database
  * Data Binding
  * Navigation
  * Glide
  * Others: Gson, material, swipetorefresh


#### Product Summary:

#### Technology Used

### Build Project

#### Using Gradle Command
To build project, you need a target environment(iOS - Mac OS / Android - Support All (Windows/Linux/Mac)).
```markdown
    # Steps 1: Clone Project
    $ git clone git@gitlab.com:indra_sapkota/disney-interview.git

    # Steps 2: Create account on <developer.marvel.com> and update private and public key
    const val API_KEY = "<API_KEY>"
    const val PRIVATE_KEY = "<PRIVATE_KEY>"

    # Steps 3: Run gradle build command
    $ cd disney-interview
    $ ./gradlew assembleDebug           // build debug build :: gradlew build (For windows)
    $ ./gradlew assembleRelease         // build release build :: gradlew build (For windows)

    # Steps 4: Verify build output
    build will be generated under -> app/build/outputs/apk/<free>/<debug>/app-free-debug.apk
```

#### Using Android Studio
Once you setup the Android Development Environment it's very easy to generate build using Android Studio.

- [Setup Android Environment](https://developer.android.com/training/basics/firstapp/creating-project)
- [Run build on Emulator or Device](https://developer.android.com/training/basics/firstapp/running-app)

```markdown
    # Steps 1: Clone Project
    $ git clone git@gitlab.com:indra_sapkota/disney-interview.git

    # Steps 2: Open Android Studio

    # Step 3: Choose Open Android Project, select `disney-interview` project from your git clone location.

    # Steps 2: Create account on <developer.marvel.com> and update private and public key
    const val API_KEY = "<API_KEY>"
    const val PRIVATE_KEY = "<PRIVATE_KEY>"

    # Step 4: Instruction to run your app on Emulator or real device
        - Follow guideline for {Run build on Emulator or Device}
```

